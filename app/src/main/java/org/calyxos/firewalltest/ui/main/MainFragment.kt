package org.calyxos.firewalltest.ui.main

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.calyxos.firewalltest.R
import org.calyxos.firewalltest.util.BeaconUtils
import java.util.*

internal const val CHANNEL_ID = "NETWORK CHANGE"

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val context = requireContext()
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        createNotificationChannel()

        requestPostNotificationsPermission()

        viewModel.statusMessage.observe(this) { value ->
            val textView = activity?.findViewById<TextView>(R.id.message)!!
            val timestampedValue = "${Date()}\n$value"
            textView.text = if (textView.text.isNullOrEmpty()) {
                timestampedValue
            } else {
                timestampedValue + "\n\n" + textView.text
            }
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.POST_NOTIFICATIONS
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                val builder = NotificationCompat.Builder(requireContext(), CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle("Network change")
                    .setContentText(value)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                with(NotificationManagerCompat.from(requireContext())) {
                    notify(42, builder.build())
                }
            }
        }

        CoroutineScope(Dispatchers.IO).launch {
            var lastResultSuccess = false
            while (true) {
                val result = BeaconUtils.massBeaconUntilChange(lastResultSuccess)
                lastResultSuccess = result.second == null
                viewModel.statusMessage.postValue("background: " + result.first)
            }
        }
        viewModel.statusMessage.postValue("Please wait for initial launch tests to finish...")
        viewModel.statusMessage.postValue("""
            ********************
            *** PLEASE READ! ***
            ********************
            logcat works better! just grep for BeaconUtils.
            Sometimes messages don't show up here, i.e. if this app isn't active.
            Also, try setting my battery usage to Unrestricted.
            *********************
            """.trimIndent())
        CoroutineScope(Dispatchers.IO).launch {
            viewModel.beacon("First launch")
            delay(3000L)
            viewModel.beacon("3s after first launch")
            delay(2000L)
            viewModel.statusMessage.postValue("Launch tests done, watching network changes...")
            viewModel.startWatchingNetworkChanges(context)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    private fun requestPostNotificationsPermission() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requireActivity().registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) {
                // no-op
            }.launch(Manifest.permission.POST_NOTIFICATIONS)
        }
    }

    private fun createNotificationChannel() {
        val name = getString(R.string.channel_name)
        val descriptionText = getString(R.string.channel_description)
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
            description = descriptionText
            setSound(null, null)
        }
        // Register the channel with the system
        val notificationManager: NotificationManager =
            requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }
}