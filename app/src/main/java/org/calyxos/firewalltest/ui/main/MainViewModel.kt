package org.calyxos.firewalltest.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.calyxos.firewalltest.util.BeaconUtils

internal const val BEACON_HOST = "1.1.1.1"

class MainViewModel : ViewModel() {
    val statusMessage: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    fun startWatchingNetworkChanges(context: Context) {
        viewModelScope.launch {
            delay(6000L)
            context.getSystemService(ConnectivityManager::class.java)
                .registerDefaultNetworkCallback(
                    object : NetworkCallback() {
                        override fun onCapabilitiesChanged(
                            network: Network,
                            networkCapabilities: NetworkCapabilities
                        ) {
                            reactToNetworkChange(networkCapabilities)
                        }
                    }
            )
        }
    }
    private val NetworkCapabilities.transports: String
        get() {
            val transports = mapOf(
                NetworkCapabilities.TRANSPORT_BLUETOOTH to "BLUETOOTH",
                NetworkCapabilities.TRANSPORT_CELLULAR to "CELLULAR",
                NetworkCapabilities.TRANSPORT_ETHERNET to "ETHERNET",
                NetworkCapabilities.TRANSPORT_USB to "USB",
                NetworkCapabilities.TRANSPORT_LOWPAN to "LOWPAN",
                NetworkCapabilities.TRANSPORT_VPN to "VPN",
                NetworkCapabilities.TRANSPORT_WIFI to "WIFI",
                NetworkCapabilities.TRANSPORT_WIFI_AWARE to "WIFI_AWARE",
            )
            val sb: StringBuilder = StringBuilder()
            for (transport in transports) {
                if (hasTransport(transport.key)) {
                    if (sb.isEmpty()) {
                        sb.append(transport.value)
                    } else {
                        sb.append(", ")
                        sb.append(transport.value)
                    }
                }
            }
            return sb.toString()
        }

    fun reactToNetworkChange(networkCapabilities: NetworkCapabilities) {
        BeaconUtils.lastTransports = networkCapabilities.transports
        viewModelScope.launch {
            val result = beaconInternal()
            val status = result.first
            statusMessage.postValue("${status}\nNetwork Type: ${networkCapabilities.transports}")
            delay(3000L)
            beacon("3s after change to ${networkCapabilities.transports}")
        }
    }

    fun beacon(description: String) {
        viewModelScope.launch {
            val result = beaconInternal()
            val status = "$description:\n" + if (result.second == null) {
                "Successful transmission: ${result.first}"
            } else {
                "Failed transmission: ${result.first}"
            }
            statusMessage.postValue("${status}\n")
        }
    }

    /**
     * Check if a host is reachable. This access attempt can be monitored from another device,
     * i.e. a router, to determine the route the network traffic ends up taking. More importantly,
     * this should fail when the app's toggle representing the active network is off.
     * Uses a TTL of 1 to receive a swift response from the nearest router; TTL exceeded is still
     * a response from another device, so it counts.
     *
     * Returns an exception if the beacon fails, or null if it succeeds.
     */
    private suspend fun beaconInternal(): Pair<String, Exception?> {
        val newValue: Pair<String, Exception?>
        withContext(Dispatchers.IO) {
            newValue = BeaconUtils.beacon()
        }
        return newValue
    }
}