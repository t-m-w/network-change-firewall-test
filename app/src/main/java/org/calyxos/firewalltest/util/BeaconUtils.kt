package org.calyxos.firewalltest.util

import android.util.Log
import kotlinx.coroutines.delay
import org.calyxos.firewalltest.ui.main.BEACON_HOST

class BeaconUtils {
    companion object {
        const val TAG = "BeaconUtils"
        // When the firewall blocks traffic, packet loss is not the reason given for a failed ping,
        // so we may want to treat it as a success.
        const val PACKET_LOSS_IS_SUCCESS = true

        var lastTransports: String = "transports unknown"

        fun beacon(log: Boolean = true): Pair<String, Exception?> {
            return try {
                val proc = ProcessBuilder().command(
                    "/system/bin/ping", "-c1",
                    "-w1", "-W1", "-s16", "-t1", BEACON_HOST
                ).redirectErrorStream(true).start()
                val input = String(proc.inputStream.readAllBytes()).trimEnd()
                proc.waitFor()
                if (proc.exitValue() == 0 ||
                    (proc.exitValue() == 1 && input.contains("exceeded"))
                ) {
                    val msg = "Successful transmission <$lastTransports>"
                    if (log) Log.i(TAG, msg)
                    Pair(msg, null)
                } else if (PACKET_LOSS_IS_SUCCESS && input.contains("100% packet loss")) {
                    val msg = "Successful transmission <$lastTransports> (packet loss)"
                    if (log) Log.i(TAG, msg)
                    Pair(msg, null)
                } else {
                    val msg = "Failed transmission <$lastTransports> (maybe?): $input"
                    if (log) Log.i(TAG, msg)
                    Pair(msg, Exception(input))
                }
            } catch (e: Exception) {
                val msg = "Failed transmission <$lastTransports>: $e"
                if (log) Log.i(TAG, msg)
                Pair(msg, e)
            }
        }

        suspend fun massBeaconUntilChange(initialSuccess: Boolean? = null):
                Pair<String, Exception?> {
            var expectSuccess: Boolean? = null
            while (true) {
                val output = beacon(log = false)
                if (expectSuccess == null) {
                    expectSuccess = initialSuccess ?: (output.second == null)
                }
                if ((output.second == null) != expectSuccess) {
                    Log.i(TAG, "background ${output.first}")
                    return output
                }
                delay(5L)
            }
        }
    }
}