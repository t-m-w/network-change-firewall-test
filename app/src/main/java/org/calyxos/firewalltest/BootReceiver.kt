package org.calyxos.firewalltest

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.calyxos.firewalltest.util.BeaconUtils

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == Intent.ACTION_BOOT_COMPLETED) {
            BeaconUtils.beacon()
        }
    }
}